import com.dev.Person;
import com.dev.Student;
import com.dev.Staff;

public class App {
    public static void main(String[] args) throws Exception {

        // khai báo đối tượng person
        Person person1 = new Person("Huong", "Ho CHi Minh City");
        Person person2 = new Person("Katty", "Usa");

        System.out.println("Person 1");
        System.out.println(person1.toString());
        System.out.println("Person 2");
        System.out.println(person2.toString());

        // khai báo đối tượng Student
        Student student1 = new Student("Mai", "Ha Noi");
        Student student2 = new Student("Katty", "Usa", "Math", 2019, 200000);

        System.out.println("Student 1");
        System.out.println(student1.toString());
        System.out.println("Student 2");
        System.out.println(student2.toString());

        // khai báo đối tượng Staff
        Staff staff1 = new Staff("Jean", "Autralia","VN HighSchool", 3000000);
        Staff staff2 = new Staff("Min", "Usa", "HCM University", 2000000);

        System.out.println("Staff 1");
        System.out.println(staff1.toString());
        System.out.println("Staff 2");
        System.out.println(staff2.toString());
    }
}
