package com.dev;

public class Person {
    String name;
    String address;

    // khởi tạo phương thức

    public Person(String name, String address) {
        this.name = name;
        this.address = address;
    }

    //getter
    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    //setter
    public void setAddress(String address) {
        this.address = address;
    }
    // in ra console
    @Override
    public String toString() {
        return "Person[name = " + this.name + ", address = " + this.address + "]";
    }
    
}
