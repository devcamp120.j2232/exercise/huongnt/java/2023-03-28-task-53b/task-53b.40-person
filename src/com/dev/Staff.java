package com.dev;

public class Staff extends Person {
    String school;
    double pay;

    // khởi tạo phương thức
    public Staff(String name, String address, String school, double pay) {
        super(name, address);
        this.school = school;
        this.pay = pay;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public double getPay() {
        return pay;
    }

    public void setPay(double pay) {
        this.pay = pay;
    }

    // in ra console
    @Override
    public String toString() {
        return "Staff [person = [ name =" + this.getName() + ", address = " + this.getAddress() + "]" + ", school = "
                + this.school + ", pay = " + this.pay + "]";
    }

}
