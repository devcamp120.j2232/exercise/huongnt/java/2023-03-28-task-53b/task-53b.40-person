package com.dev;

public class Student extends Person {
    Person person;
    String program;
    int year;
    double fee;


    //khởi tạo phương thức
    public Student(String name, String address) {
        super(name, address);
    }


    public Student(String name, String address, String program, int year, double fee) {
        super(name, address);
        this.program = program;
        this.year = year;
        this.fee = fee;
    }

    //getter & setter
    public String getProgram() {
        return program;
    }


    public void setProgram(String program) {
        this.program = program;
    }


    public int getYear() {
        return year;
    }


    public void setYear(int year) {
        this.year = year;
    }


    public double getFee() {
        return fee;
    }


    public void setFee(double fee) {
        this.fee = fee;
    }
    
        // in ra console
        @Override
        public String toString() {
            return "Student[person = [ name =" + this.getName() + ", address = " + this.getAddress() + "]"  + ", program = " + this.program + ", year = " + this.year +" , fee = " + this.fee +"]";
        }
        
    
}
  
